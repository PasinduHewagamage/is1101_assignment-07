#include<stdio.h>

int main()
{
	int a[3][3], b[3][3], c[3][3]={0}, d[3][3]={0};
	int i,j,k,m,n,p,q;

	printf("Enter no. of rows and columns in 1st matrix : ");
	scanf("%d%d",&m,&n);
	printf("Enter no. of rows and columns in 2nd matrix : ");
	scanf("%d%d",&p,&q);

	if(m!=p || n!=q)
	{
		printf("Matrix Addition is not possible");
		return;
	}
	else if(n!=p)
	{
		printf("Matrix Multiplication is not possible");
		return;
	}
	else
	{
		printf("Enter elements of 1st matrix : ");
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				scanf("%d", &a[i][j]);

		printf("Enter elements of 2nd matrix : ");
		for(i=0;i<p;i++)
			for(j=0;j<q;j++)
				scanf("%d", &b[i][j]);


		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				c[i][j] = a[i][j] + b[i][j];
		printf("\nResult of matrix addition:\n");

		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}


		for(i=0;i<m;i++)
			for(j=0;j<q;j++)
				for(k=0;k<p;k++)
					d[i][j] += a[i][k]*b[k][j];
		printf("\nResult of matrix multiplication:\n");

		for(i=0;i<m;i++)
		{
			for(j=0;j<q;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	}

	return 0;
}
