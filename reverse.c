#include <stdio.h>
#include <string.h>
#define SIZE 1000
int main()
{
   char s[SIZE];

   printf("Enter a sentence to reverse\n");
   gets(s);

   strrev(s);

   printf("Reverse of the sentence: %s\n", s);

   return 0;
}

